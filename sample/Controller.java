package sample;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Controller {

    @FXML
    private Canvas canvas;

    @FXML
    private ColorPicker colorPicker;

    @FXML
    private CheckBox erase;

    @FXML
    private GraphicsContext graphicsContext;

    @FXML
    private Slider slider;

    @FXML
    private Label label;

    public void initialize() {
        graphicsContext = canvas.getGraphicsContext2D();

        slider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                label.setText(String.format("%.0f", newValue));
                graphicsContext.setLineWidth(slider.getValue());
            }
        });

        colorPicker.setOnAction(event -> {
            graphicsContext.setStroke(colorPicker.getValue());

        });

        canvas.setOnMousePressed(event -> {
            if (erase.isSelected()) {
                graphicsContext.clearRect(event.getX(), event.getY(),
                        slider.getValue(), slider.getValue());
            } else {
                graphicsContext.beginPath();
                graphicsContext.lineTo(event.getX(), event.getY());
                graphicsContext.stroke();
            }

        });

        canvas.setOnMouseDragged(e -> {
            slider.setMin(1);
            slider.setMax(50);
            if (erase.isSelected()) {
                graphicsContext.clearRect(e.getX(), e.getY(), slider.getValue(), slider.getValue());
            } else {
                graphicsContext.lineTo(e.getX(), e.getY());
                graphicsContext.stroke();
            }

        });
    }

    public void onSave() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PNG files (*.png)", "*.png"),
                new FileChooser.ExtensionFilter("JPEG files (*.jpeg)", "*.jpeg"),
                new FileChooser.ExtensionFilter("BMP files (*.bmp)", "*.bmp"),
                new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.gif"));

        fileChooser.setInitialFileName("untitled");
        fileChooser.setInitialDirectory(new File("C:\\DevEducation"));
        File file = fileChooser.showSaveDialog(null);

        if (file != null) {
            try {
                WritableImage writableImage = canvas.snapshot(null, null);
                ImageIO.write(SwingFXUtils.fromFXImage(writableImage, null), "png", file);
            } catch (IOException e) {
                System.out.println("Failed to save image" + e);
            }
            graphicsContext.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        }
    }

    public void onExit() {
        Platform.exit();
    }

    public void onOpen() {
        FileChooser openFile = new FileChooser();
        openFile.setTitle("Open File");
        File file = openFile.showOpenDialog(null);
        if (file != null) {
            try {
                InputStream io = new FileInputStream(file);
                Image img = new Image(io);
                graphicsContext.drawImage(img, 0, 0);
            } catch (IOException ex) {
                System.out.println("Error!");
            }
        }
    }

    public void clearAll() {
        graphicsContext.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
    }
}

